#include "description.h"

#include <QInputDialog>
#include <QDebug>

#include "Task.h"
#include "ui_Task.h"
/*
Description::Description(const QString& name, QWidget *parent) :
        QWidget(parent),
        ui(new Ui::Description)
{
    ui->setupUi(this);
    setName(name);

    connect(ui->editButton, &QPushButton::clicked, this, &Task::rename);
    connect(ui->removeButton, &QPushButton::clicked, [this] {
        emit removed(this);
    });
    connect(ui->checkbox, &QCheckBox::toggled, this, &Task::checked);
}

Description::~Description()
{
    qDebug() << "~Task() called";
    delete ui;
}

void Description::setName(const QString& name)
{
    ui->checkbox->setText(name);
}

QString Description::name() const
{
    return ui->checkbox->text();
}

bool Description::isCompleted() const
{
   return ui->checkbox->isChecked();
}

void Description::rename()
{
    bool ok;
    QString value = QInputDialog::getText(this, tr("Edit task"),
                                          tr("Task name"), QLineEdit::Normal,
                                          this->name(), &ok);
    if (ok && !value.isEmpty()) {
        setName(value);
    }
}

void Description::checked(bool checked)
{
    QFont font(ui->checkbox->font());
    font.setStrikeOut(checked);
    ui->checkbox->setFont(font);

    emit statusChanged(this);
}
*/
